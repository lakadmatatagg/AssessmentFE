import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import * as bootstrap from 'bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var window:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent implements OnInit {
  // All
  lists: any; users: any; statuss: any; edit:any; create:any;

  // Modal
  modal:any;
  alertmodal:any;
  toast: any;

  // Form
  timesheetForm!: FormGroup;
  submitted = false;

  // Sort
  sort: string = "DESC";
  field: any;
  
  constructor (private service: DataService, private formBuilder: FormBuilder) {}

  ngOnInit(): void {

    // Initialise 
    this.refreshList(); // List (disable b4 meeting)
    this.getStatus(); // Status dropdown
    this.getUser(); // User dropdown
    this.formInit(); // form
    this.modal = new bootstrap.Modal("#formModal"); // Modal
    this.alertmodal = new bootstrap.Modal("#alertModal"); // Alert Modal
    this.toast = new bootstrap.Toast("#toast");

    // Modal onhide
    $("#formModal").on("hidden.bs.modal", () => {
      window.document.getElementById('task_search').value='';
      this.timesheetForm.controls['sheet_id'].setValue("");
      this.timesheetForm.controls['project'].setValue("");
      this.timesheetForm.controls['task'].setValue("");
      this.timesheetForm.controls['date_from'].setValue("");
      this.timesheetForm.controls['date_to'].setValue("");
      this.timesheetForm.controls['status'].setValue("");
      this.timesheetForm.controls['assign_to'].setValue("");
    });
  }

  openModal(id:any) {
    this.submitted = false;
    // Create Mode
    const modalTitle = window.document.getElementById("formModalLabel");
    if (!id) {
      modalTitle.innerText = "Timesheet Entry";
      this.timesheetForm.controls['sheet_id'].setValue("");
      this.timesheetForm.controls['project'].setValue("");
      this.timesheetForm.controls['task'].setValue("");
      this.timesheetForm.controls['date_from'].setValue("");
      this.timesheetForm.controls['date_to'].setValue("");
      this.timesheetForm.controls['status'].setValue("");
      this.timesheetForm.controls['assign_to'].setValue("");
    // Edit Mode
    } else {
      this.service.getListById(id).subscribe((data) => {
        this.edit = data;
        modalTitle.innerText = "Task: " + this.edit['task'];
        this.timesheetForm.controls['sheet_id'].setValue(this.edit['id']);
        this.timesheetForm.controls['project'].setValue(this.edit['project']);
        this.timesheetForm.controls['task'].setValue(this.edit['task']);
        this.timesheetForm.controls['date_from'].setValue(this.edit['dateFrom']);
        this.timesheetForm.controls['date_to'].setValue(this.edit['dateTo']);
        this.timesheetForm.controls['status'].setValue(this.edit['status_id']);
        this.timesheetForm.controls['assign_to'].setValue(this.edit['user_id']);
      });
    }
    this.modal.show();
  }

  openAlertModal(id:any, project:any) {
    if (id) {
      this.alertmodal.show();
      window.document.getElementById("delete_id").value = id;
      window.document.getElementById("alertModalLabel").innerText = "Delete Project: " + project;
    } else {
      window.document.getElementById("action_toast").innerText = "Data Error";
      window.document.getElementById("detail_toast").innerText = "Could not retrieve Timesheet Identifier. Please contact administrator";
      this.toast.show();
    }
  }

  sortBy(field:any) {
    if (!this.field) {
      this.field = field
    } else {
      if (this.field == field) {
        if (this.sort == "ASC") {
          this.sort = "DESC";
        } else {
          this.sort = "ASC";
        }
      } else {
        if (this.sort == "ASC") {
          this.sort = "DESC";
        } else {
          this.sort = "DESC";
        }
      }
    }
    
    window.document.getElementById(this.field+"_icon").innerHTML = '';
    if (this.sort == "ASC") {
      window.document.getElementById(field+"_icon").innerHTML = '<i class="bi bi-sort-up"></i>';
    } else {
      window.document.getElementById(field+"_icon").innerHTML = '<i class="bi bi-sort-down"></i>';
    }
    this.field = field;

    var search = window.document.getElementById("task_search").value;
    if (!search) {
      this.service.getListSorted(this.field, this.sort).subscribe((data) => {
        this.lists = data;
      });
    } else {
      this.service.getListByTaskSorted(search, this.field, this.sort).subscribe((data) => {
        this.lists = data;
      });
    }
  }

  getStatus() {
    this.service.getStatus().subscribe((data) => {
      this.statuss = data;
      var initUser = {
        "id" : "",
        "statusName" : "-- Please Select --"
      };
      this.statuss.unshift(initUser);
    });
  }

  getUser() {
    this.service.getUser().subscribe((data) => {
      this.users = data;
      var initUser = {
        "id" : "",
        "name" : "-- Please Select --"
      };
      this.users.unshift(initUser);
    });
  }

  refreshList() {
    if (this.field) {
      window.document.getElementById(this.field+"_icon").innerHTML = '';
    }
    this.service.getList().subscribe((data) => {
      this.lists = data;
    });
  }

  formInit() {
    this.timesheetForm = this.formBuilder.group({
      sheet_id:[''],
      project:['', Validators.required],
      task:['', Validators.required],
      date_from:['', Validators.required],
      date_to:['', Validators.required],
      status:['', Validators.required],
      assign_to:['', Validators.required],
    });
  }

  modalClose() {
    this.modal.hide();
  }

  search() {
    const action_toast = window.document.getElementById("action_toast");
    const detail_toast = window.document.getElementById("detail_toast");
    var task = window.document.getElementById("task_search").value;
    if (!task) {
      this.refreshList();
    } else {
      this.service.getListByTask(task).subscribe((data) => {
        this.lists = data;

        if (this.lists.length == 0) {
          action_toast.innerText = "Search Timesheet";
          detail_toast.innerText = "Couldn't find any result for " + task;
          this.toast.show();
        } else {
          action_toast.innerText = "Search Timesheet";
          detail_toast.innerText = this.lists.length + " Result found";
          this.toast.show();
        }
      });
    }
  }

  clearSearch() {
    if (this.field) {
      window.document.getElementById(this.field+"_icon").innerHTML = '';
    }
    window.document.getElementById('task_search').value='';
    this.refreshList();
  }

  deleteTimesheet() {
    var id = window.document.getElementById("delete_id").value;
    const action_toast = window.document.getElementById("action_toast");
    const detail_toast = window.document.getElementById("detail_toast");
    console.log(id);
    this.service.deleteTimeSheet(id).subscribe((data) => {},(http) => {
      if (http.status == 200) {
        this.refreshList();
        action_toast.innerText = "Delete Timesheet";
        detail_toast.innerText = "Timesheet has been Deleted";
        this.toast.show();
        this.alertmodal.hide();
      } else {
        console.log("Error");
        action_toast.innerText = "Delete Timesheet";
        detail_toast.innerText = "Could not delete Timesheet. Server Error";
        this.toast.show();
        this.alertmodal.hide();
      }
    });
  }

  saveForm() {

    const action_toast = window.document.getElementById("action_toast");
    const detail_toast = window.document.getElementById("detail_toast");
    this.submitted = true;

    if (this.timesheetForm.invalid) {
      return;
    } else {
      console.log(this.timesheetForm.controls["project"].value);
      // dunno how to loop, so do it manual to save time
      var data = {
        project : this.timesheetForm.controls["project"].value,
        task : this.timesheetForm.controls["task"].value,
        dateFrom : this.timesheetForm.controls["date_from"].value,
        dateTo : this.timesheetForm.controls["date_to"].value,
        user_id : this.timesheetForm.controls["assign_to"].value,
        status_id : this.timesheetForm.controls["status"].value,
      };
      console.log(data);
      // Object.keys(this.timesheetForm.controls).forEach((key) => {
      //   console.log(this.timesheetForm.controls[key].value);
      //   // data[key] = this.timesheetForm.controls[key].value;
      //   data = Object.assign(data, this.timesheetForm.controls[key].value);
      //   // $scope.data.push({"dasd" : this.timesheetForm.controls[key]});
      //   // data['sad'] = this.timesheetForm.controls[key]
      // });

      var id = this.timesheetForm.controls["sheet_id"].value;
      // No id = create mode
      if (!id) {
        this.service.createTimeSheet(data).subscribe((res) => {
          this.create = res;
          
          // should be check if status network 200 then true but takes time
          if (this.create.project == this.timesheetForm.controls["project"].value) {
            console.log("Success");
            this.refreshList();
            action_toast.innerText = "Create Timesheet";
            detail_toast.innerText = "Timesheet has been created";
            this.toast.show();
            this.modalClose();
          } else {
            console.log("Error");
          }
        });
      // With Id = edit mode
      } else {
        // rewrite data bcs need to add id inside it
        var updatedata = {
          id : this.timesheetForm.controls["sheet_id"].value,
          project : this.timesheetForm.controls["project"].value,
          task : this.timesheetForm.controls["task"].value,
          dateFrom : this.timesheetForm.controls["date_from"].value,
          dateTo : this.timesheetForm.controls["date_to"].value,
          user_id : this.timesheetForm.controls["assign_to"].value,
          status_id : this.timesheetForm.controls["status"].value,
        };

        this.service.editTimeSheet(updatedata).subscribe((res) => {
          this.create = res;
          
          // should be check if status network 200 then true but takes time
          if (this.create.id == id) {
            console.log("Success");
            this.refreshList();
            this.modalClose();
            
            action_toast.innerText = "Update Timesheet";
            detail_toast.innerText = "Timesheet has been Updated";
            this.toast.show();
          } else {
            console.log("Error");
          }
        });
      }
      
    }
  }
}
