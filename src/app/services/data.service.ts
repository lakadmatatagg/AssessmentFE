import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  constructor(private http: HttpClient) { }
  
  // Full List
  getList() {
    return this.http.get("http://localhost:6969/timesheets")
  }

  // Searach Timesheet
  getListByTask(task:any) {
    return this.http.get("http://localhost:6969/timesheetByTask/"+task)
  }

  // Sort Timesheet
  getListSorted(field:any, order:any) {
    return this.http.get("http://localhost:6969/timesheets/"+ field +"/by/"+ order +"")
  }

  // Searach and Sort Timesheet
  getListByTaskSorted(task:any, field:any, order:any) {
    return this.http.get("http://localhost:6969/timesheetByTaskSorted/" + task + "/field/" + field + "/order/" + order)
  }

  // Single Timesheet
  getListById(id:any) {
    return this.http.get("http://localhost:6969/timesheet/"+id)
  }

  // List User
  getUser() {
    return this.http.get("http://localhost:6969/users")
  }

  // List Status
  getStatus() {
    return this.http.get("http://localhost:6969/status")
  }

  // Create Timesheet
  createTimeSheet(data: Object) {
    let headers = new HttpHeaders({
        'Content-Type' : 'application/json; charset=UTF-8'
    });
    let options = {
        headers: headers
    };
    return this.http.post("http://localhost:6969/addTimesheet", data , options);
  }

  // Update Timesheet
  editTimeSheet(data: Object) {
    let headers = new HttpHeaders({
        'Content-Type' : 'application/json; charset=UTF-8'
    });
    let options = {
        headers: headers
    };
    return this.http.put("http://localhost:6969/update", data , options);
  }

  // Delete Timesheet
  deleteTimeSheet(id:any) {
    return this.http.delete("http://localhost:6969/delete/"+id)
  }
}
